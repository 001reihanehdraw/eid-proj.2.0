package src.menu;

import static src.Helper.getInput;
import static src.Helper.getIntInput;

import java.util.List;
import java.util.Random;

import src.Helper;
import src.models.College;
import src.models.Lesson;
import src.models.Professor;
import src.models.Semester;

public class EducationMenu
{
    public static void mainMenu() {
        System.out.println("\nWelcome Education!\n");
        while (true) {
            System.out.println("-------------- Education Menu --------------");
            System.out.println();
            System.out.println("Make Faculty:    To create new college");
            System.out.println("New Semester:    To create new semester");
            System.out.println("New Course:      To add lessons");
            System.out.println("Logout:          To log out");
            System.out.println();

            System.out.print("Enter your command: ");
            String cmd = getInput();
            System.out.println();
            if (cmd.equals("Logout")) break;
            switch (cmd) {
                case "Make Faculty":
                    createCollege();
                    break;
                case "New Semester":
                    createSemester();
                    break;
                case "New Course":
                    createLessons();
                    break;
            }
            System.out.println("\nPress enter to continue...");
            Helper.getInput();
        }
    }

    private static void createCollege() {
        College college = new College();

        System.out.print("Enter College Name: ");
        college.setName(getInput());

        College.addCollege(college);
        System.out.printf("\nSuccessfully created '%s' !\n", college.getName());
    }

    private static void createSemester() {
        if (College.getColleges().size() <= 0) {
            System.out.println("No colleges available!");
            return;
        }

        System.out.println();
        College college = College.getCollege();
        System.out.println();

        Random random = new Random();
        int id = random.nextInt(40000000) + 50000000;

        Semester semester = new Semester(id);
        college.setCurrentSemesterID(id);
        college.addSemester(semester);

        System.out.println("Successfully created new Semester!");
    }

    private static void createLessons() {
        if (College.getColleges().size() <= 0) {
            System.out.println("No colleges available!");
            return;
        }

        System.out.println();
        College college = College.getCollege();

        List<Lesson> currentLessons = college.getCurrentLessons();

        do {
            Lesson lesson = new Lesson();
            System.out.print("Enter Lesson Name: ");
            lesson.setName(getInput());

            System.out.print("Enter Lesson ID: ");
            lesson.setLessonID(getIntInput());

            System.out.print("Enter Lesson Unit: ");
            lesson.setUnit(getIntInput());
            System.out.println();

            lesson.setProfessor(chooseProfessor());

            currentLessons.add(lesson);

            System.out.printf("Successfully created '%s' !\n", lesson.getName());
            System.out.print("\nDo you want add more lessons? (Yes/No): ");
        } while (!Helper.getInput().equals("No"));
    }

    private static Professor chooseProfessor() {
        while (true) {
            System.out.println("Choose a professor for lesson:");
            List<Professor> professors = Professor.getProfessors();
            for (int i = 0; i < professors.size(); i++) {
                Professor p = professors.get(i);
                System.out.printf("%d: %s\n", i + 1, p.getName());
            }
            System.out.print("\nType professor number: ");
            int intInput = Helper.getIntInput() - 1;
            System.out.println();

            if (intInput >= 0 && intInput < professors.size()) {
                return professors.get(intInput);
            }
            System.out.println("Invalid number!\n");
        }
    }
}
