package src;

import static src.Helper.getInput;
import static src.Helper.getIntInput;
import static src.Helper.getLongInput;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import src.menu.EducationMenu;
import src.menu.ProfessorMenu;
import src.menu.StudentMenu;
import src.models.College;
import src.models.Education;
import src.models.Lesson;
import src.models.Professor;
import src.models.Semester;
import src.models.Student;

public class Main
{
    private static Map<String, String> userPassMap = new HashMap<>();
    private static Map<String, Student> studentMap = new HashMap<>();
    private static Map<String, Professor> professorMap = new HashMap<>();
    private static Map<String, Education> educationMap = new HashMap<>();

    public static void main(String[] args) {
        /*prepareDatabase();
        if (!true) {
            ProfessorMenu.mainMenu(professorMap.get("c0p0"));
            return;
        }*/
        while (true) {
            System.out.println();
            System.out.println("-------------- Main --------------");
            System.out.println();
            System.out.println("Please enter a command:");
            System.out.println();
            System.out.println("Sign Up:    To sign up");
            System.out.println("Login:      To sign in");
            System.out.println("Quit:       Terminate app");
            System.out.println();
            System.out.print("Enter your command: ");
            String cmd = Helper.getScanner().nextLine();
            if (cmd.equals("Quit")) {
                System.out.println("Goodbye!");
                break;
            }
            System.out.println();
            switch (cmd) {
                case "Sign Up":
                    signUp();
                    break;
                case "Login":
                    signIn();
                    break;
                case "Logout":
                    break;
                case "Edit Info":
                    break;
                default:
                    System.out.println("Enter a valid command!");
                    System.out.println();
            }
        }
    }

    private static void prepareDatabase() {
        Random random = new Random();
        for (int i = 0; i < 1; i++) {
            College college = new College();
            college.setName("College " + (i + 1));
            List<Professor> professors = new ArrayList<>();
            for (int j = 0; j < 1; j++) {
                Professor professor = new Professor();
                professor.setCollege(college);
                professor.setName("Professor (c" + i + ")(p" + j + ")");
                String username = "c" + i + "p" + j;
                String password = "pass";
                userPassMap.put(username, password);
                professorMap.put(username, professor);
                professors.add(professor);
            }
            List<Lesson> lessons = new ArrayList<>();
            for (int j = 0; j < 5; j++) {
                Lesson lesson = new Lesson();
                lesson.setName("Riazi c" + i + "l" + j);
                lesson.setLessonID(random.nextInt(1000000) + 5000000);
                lesson.setProfessor(professors.get(0));
                lesson.setUnit((j % 3) + 2);
                lessons.add(lesson);
            }
            int id = random.nextInt(40000000) + 50000000;
            college.setCurrentSemesterID(id);
            college.addSemester(new Semester(lessons, id));
            College.getColleges().add(college);
        }
        for (int i = 0; i < 50; i++) {
            Student student = new Student();
            long id = random.nextLong(4000000000L) + 5000000000L;
            student.setStudentID(id);
            student.setYear(4546);
            student.setCollege(College.getColleges().get(0));
            student.setMajor("ascjaisc");
            student.setName("Mahdi Mohammadpour");
            String username = "st" + i;
            String password = "pass";
            College college = College.getColleges().get(0);
            List<Lesson> currentLessons = college.getCurrentLessons();
            for (Lesson lesson : currentLessons) {
                lesson.getProfessor().addStudent(lesson, student);
            }
            student.addSemester(new Semester(currentLessons, college.getCurrentSemesterID()));
            userPassMap.put(username, password);
            studentMap.put(username, student);
        }
    }

    private static void signUp() {
        System.out.println("-------------- Sign Up --------------");
        System.out.println();
        System.out.println("Please enter a command:");
        System.out.println();
        System.out.println("Student:    Student user");
        System.out.println("Professor:  Professor user");
        System.out.println("Education:  Education");
        System.out.println();
        System.out.print("Enter your command: ");
        String type = getInput();
        System.out.println();
        if (type.equals("Education") && educationMap.size() == 1) {
            System.out.print("Education account is already exists!");
            return;
        }

        System.out.print("Enter username: ");
        String username = getInput();
        if (userPassMap.containsKey(username)) {
            System.out.print("Username is already exists!");
            return;
        }

        System.out.print("Enter password: ");
        String password = getInput();
        System.out.println();

        switch (type) {
            case "Student":
                Student student = createStudent();
                userPassMap.put(username, password);
                student = StudentMenu.mainMenu(student);
                studentMap.put(username, student);
                break;
            case "Professor":
                Professor professor = createProfessor();
                userPassMap.put(username, password);
                professor = ProfessorMenu.mainMenu(professor);
                professorMap.put(username, professor);
                Professor.addProfessor(professor);
                break;
            case "Education":
                userPassMap.put(username, password);
                educationMap.put(username, new Education());
                EducationMenu.mainMenu();
                break;
        }
    }

    private static void signIn() {
        System.out.println("-------------- Sign In --------------");
        System.out.println();
        System.out.println("Please enter a command:");
        System.out.println();
        System.out.println("Student:    Student user");
        System.out.println("Professor:  Professor user");
        System.out.println("Education:  Education");
        System.out.println();
        System.out.print("Enter your command: ");
        String type = getInput();
        System.out.println();

        System.out.print("Enter username: ");
        String username = getInput();
        if (!userPassMap.containsKey(username)) {
            System.out.print("Username doesn't exists!");
            return;
        }

        System.out.print("Enter password: ");
        String password = getInput();
        System.out.println();

        if (!userPassMap.get(username).equals(password)) {
            System.out.print("Password doesn't match!");
            return;
        }

        switch (type) {
            case "Student":
                Student student = StudentMenu.mainMenu(studentMap.get(username));
                studentMap.put(username, student);
                break;
            case "Professor":
                Professor professor = ProfessorMenu.mainMenu(professorMap.get(username));
                professorMap.put(username,professor);
                break;
            case "Education":
                EducationMenu.mainMenu();
                break;
        }
    }

    private static Student createStudent() {
        Student student = new Student();
        System.out.print("Enter Your Name: ");
        student.setName(getInput());

        System.out.print("Enter Student ID: ");
        student.setStudentID(getLongInput());

        System.out.print("Enter Major: ");
        student.setMajor(getInput());

        System.out.print("Enter year: ");
        student.setYear(getIntInput());

        if (College.getColleges().size() > 0) {
            System.out.println();
            student.setCollege(College.getCollege());
            System.out.println();
        } else {
            System.out.println("No colleges available!");
        }

        return student;
    }

    private static Professor createProfessor() {
        Professor professor = new Professor();
        System.out.print("Enter Your Name: ");
        professor.setName(getInput());

        if (College.getColleges().size() > 0) {
            System.out.println();
            professor.setCollege(College.getCollege());
            System.out.println();
        } else {
            System.out.println("No colleges available!");
        }

        return professor;
    }
}
