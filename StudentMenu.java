package src.menu;

import static src.Helper.getInput;
import static src.Helper.getIntInput;
import static src.Helper.getLongInput;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import src.Helper;
import src.models.College;
import src.models.Lesson;
import src.models.Semester;
import src.models.Student;

public class StudentMenu
{
    private static Student student;

    public static Student mainMenu(Student student) {
        StudentMenu.student = student;
        System.out.println("\nWelcome " + student.getName() + "!\n");
        while (true) {
            System.out.println("-------------- Student Menu --------------");
            System.out.println();
            System.out.println("Register:   For select courses");
            System.out.println("Scores:     See grades");
            System.out.println("Schedule:   Show selected courses");
            System.out.println("Edit Info:  To change user info");
            System.out.println("Logout:     To log out");
            System.out.println();

            System.out.print("Enter your command: ");
            String cmd = getInput();
            System.out.println();
            if (cmd.equals("Logout")) break;
            switch (cmd) {
                case "Register":
                    selectUnit();
                    break;
                case "Scores":
                    showScores();
                    break;
                case "Schedule":
                    showUnits();
                    break;
                case "Edit Info":
                    editInfo();
                    break;
            }
            System.out.println("\nPress enter to continue...");
            Helper.getInput();
        }
        return StudentMenu.student;
    }

    private static boolean selectUnit() {
        College college = student.getCollege();
        if (college != null) {
            List<Lesson> lessons = college.getCurrentLessons();
            if (lessons.size() > 0) {
                float previousAverage = student.getPreviousAverage();
                if (previousAverage >= 17) {
                    System.out.println("*Your grade of previous semester is A");
                    System.out.println("*You can select units up to 24\n");
                }

                List<Lesson> selectedUnits = new ArrayList<>();
                boolean finished = false;
                do {
                    selectedUnits.clear();
                    System.out.println("Lessons list:");
                    for (int i = 0; i < lessons.size(); i++) {
                        Lesson lesson = lessons.get(i);
                        System.out.printf("%d: (%d) %s (%d)\n", i + 1, lesson.getLessonID(), lesson.getName(), lesson.getUnit());
                    }
                    System.out.println();

                    System.out.print("Type unit numbers by space (ex: 1 2 3 4 5) (type 0 to go back): ");
                    String cmd = getInput();
                    if (cmd.equals("0")) {
                        return false;
                    }
                    System.out.println();

                    String[] l = cmd.split(" ");
                    int units = 0;
                    for (String a : l) {
                        int index = Integer.parseInt(a) - 1;
                        if (index >= 0 && index < lessons.size()) {
                            Lesson lesson = lessons.get(index);
                            selectedUnits.add(lesson);
                            units += lesson.getUnit();
                        }
                    }

                    if (units > 12) {
                        if (previousAverage >= 17) {
                            if (units <= 24) {
                                finished = true;
                            } else {
                                System.out.println("Selected units are bigger than 24!\n");
                            }
                        } else if (units <= 20) {
                            finished = true;
                        } else {
                            System.out.println("Selected units are bigger than 20!\n");
                        }
                    } else {
                        System.out.println("Selected units are smaller than 12!\n");
                    }
                } while (!finished);
                for (Lesson lesson : selectedUnits) {
                    lesson.getProfessor().addStudent(lesson, student);
                }
                student.addSemester(new Semester(selectedUnits, college.getCurrentSemesterID()));
                System.out.println("Done!");
            } else {
                System.out.println("Semester isn't defined by Education!");
            }
        }
        return true;
    }

    private static void showScores() {
        List<Lesson> lessons = student.getCurrentLessons();
        if (lessons.size() <= 0) {
            System.out.println("You haven't chosen your courses!");
            return;
        }
        float average = 0;
        int totalUnits = 0;
        for (int i = 0; i < lessons.size(); i++) {
            Lesson l = lessons.get(i);
            int unit = l.getUnit();
            float grade = student.getGrade(l);
            average += (grade * unit);
            totalUnits += unit;
            System.out.printf("%.2f -> %s (%d)\n", grade, l.getName(), unit);
        }
        System.out.printf("Your average is : %.2f\n", average / totalUnits);

    }

    private static void showUnits() {
        List<Lesson> selectedUnits = student.getCurrentLessons();
        if (selectedUnits.size() <= 0) {
            System.out.println("You haven't chosen your courses!");
            return;
        }
        System.out.println("Selected units:");
        for (int i = 0; i < selectedUnits.size(); i++) {
            Lesson lesson = selectedUnits.get(i);
            System.out.printf("%d: (%d) %s (%d)\n", i + 1, lesson.getLessonID(), lesson.getName(), lesson.getUnit());
        }
    }

    private static void editInfo() {
        while (true) {
            System.out.println("Edit student info:");
            int i = 0;
            System.out.printf("%d: (%s) %s\n", ++i, "Name", student.getName());
            System.out.printf("%d: (%s) %s\n", ++i, "ID", student.getStudentID());
            System.out.printf("%d: (%s) %s\n", ++i, "Major", student.getMajor());
            System.out.printf("%d: (%s) %s\n", ++i, "Year", student.getYear());
            System.out.println();

            System.out.print("Type info number (type 0 to go back): ");
            int intInput = Helper.getIntInput();

            if (intInput == 0) {
                break;
            }
            intInput--;
            System.out.println();
            if (intInput < 0 || intInput >= 4) {
                System.out.println("Invalid number!\n");
                continue;
            }
            switch (intInput) {
                case 0:
                    System.out.print("Enter Your Name: ");
                    student.setName(getInput());
                    break;
                case 1:
                    System.out.print("Enter Student ID: ");
                    student.setStudentID(getLongInput());
                    break;
                case 2:
                    System.out.print("Enter Major: ");
                    student.setMajor(getInput());
                    break;
                case 3:
                    System.out.print("Enter year: ");
                    student.setYear(getIntInput());
            }
            System.out.println();
        }
    }
}
