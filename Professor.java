package src.models;

import static src.Helper.getInput;
import static src.Helper.getIntInput;
import static src.Helper.getLongInput;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import src.Helper;

public class Professor
{
    private static List<Professor> professors = new ArrayList<>();
    private String name;
    private College college;
    private HashMap<Lesson, List<Student>> students = new HashMap<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public College getCollege() {
        return college;
    }

    public void setCollege(College college) {
        this.college = college;
    }

    public void addStudent(Lesson lesson, Student student) {
        List<Student> stlist = students.get(lesson);
        if (stlist == null) {
            stlist = new ArrayList<>();
        }
        stlist.add(student);
        students.put(lesson, stlist);
    }

    public List<Student> getStudents(Lesson l) {
        return students.getOrDefault(l, new ArrayList<>());
    }

    public static void addProfessor(Professor p) {
        professors.add(p);
    }

    public static List<Professor> getProfessors() {
        return professors;
    }
}
