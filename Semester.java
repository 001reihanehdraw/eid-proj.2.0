package src.models;

import java.util.ArrayList;
import java.util.List;

public class Semester
{
    private List<Lesson> lessons = new ArrayList<>();
    private int id;

    public Semester(int id) {
        this.id = id;
    }

    public Semester(List<Lesson> lessons, int id) {
        this.lessons = lessons;
        this.id = id;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    public boolean isCurrentSemester(College college) {
        return id == college.getCurrentSemesterID();
    }
}
