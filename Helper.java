package src;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Helper
{
    private static Scanner scanner;

    private static void checkScanner() {
        if (scanner == null) scanner = new Scanner(System.in);
    }

    public static Scanner getScanner() {
        checkScanner();
        return scanner;
    }

    public static String getInput() {
        checkScanner();
        String cmd = scanner.nextLine();
        if (cmd.equals("Quit")) {
            scanner.close();
            System.out.println("Goodbye!");
            System.exit(0);
            return null;
        }
        return cmd;
    }

    public static int getIntInput() {
        checkScanner();
        String cmd = scanner.nextLine();
        if (cmd.equals("Quit")) {
            scanner.close();
            System.out.println("Goodbye!");
            System.exit(0);
        }
        try {
            return Integer.parseInt(cmd);
        } catch (Exception ignored) {
        }
        return -1;
    }

    public static long getLongInput() {
        checkScanner();
        String cmd = scanner.nextLine();
        if (cmd.equalsIgnoreCase("quit")) {
            scanner.close();
            System.out.println("Goodbye!");
            System.exit(0);
        }
        try {
            return Long.parseLong(cmd);
        } catch (Exception ignored) {
        }
        return -1;
    }

    public static float getFloatInput() {
        checkScanner();
        String cmd = scanner.nextLine();
        if (cmd.equals("Quit")) {
            scanner.close();
            System.out.println("Goodbye!");
            System.exit(0);
        }
        try {
            return Float.parseFloat(cmd);
        } catch (Exception ignored) {
        }
        return -1;
    }
}
