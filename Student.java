package src.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Student
{
    private String name;
    private long studentID;
    private String major;
    private College college;
    private int year;
    private List<Semester> semesters = new ArrayList<>();
    private HashMap<Lesson, Float> grades = new HashMap<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStudentID() {
        return studentID;
    }

    public void setStudentID(long studentID) {
        this.studentID = studentID;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public College getCollege() {
        return college;
    }

    public void setCollege(College college) {
        this.college = college;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void addSemester(Semester semester) {
        semesters.add(0, semester);
    }

    public List<Lesson> getCurrentLessons() {
        if (semesters.size() > 0) {
            Semester semester = semesters.get(0);
            if (semester.isCurrentSemester(college)) {
                return semester.getLessons();
            }
        }
        return new ArrayList<>();
    }

    public void addGrade(Lesson lesson, float grade) {
        grades.put(lesson, grade);
    }

    public float getGrade(Lesson lesson) {
        return grades.getOrDefault(lesson, 0f);
    }

    public float getPreviousAverage() {
        if (semesters.size() > 0) {
            Semester semester = semesters.get(0);
            if (!semester.isCurrentSemester(college)) {
                return getAverage(semester.getLessons());
            } else if (semesters.size() > 1) {
                return getAverage(semesters.get(1).getLessons());
            }
        }
        return -1;
    }

    public float getAverage(List<Lesson> lessons) {
        float grade = 0;
        int unit = 0;
        for (Lesson l : lessons) {
            float g = grades.getOrDefault(l, 0f);
            int u = l.getUnit();
            grade += (g * u);
            unit += u;
        }
        return grade/unit;
    }
}
