package src.models;

import java.util.ArrayList;
import java.util.List;

import src.Helper;
import src.Main;

public class College
{
    private static List<College> colleges = new ArrayList<>();
    private String name;
    private List<Semester> semesters = new ArrayList<>();
    private int currentSemesterID;

    public static College getCollege() {
        while (true) {
            System.out.println("College list:");
            int size = colleges.size();
            for (int i = 0; i < size; i++) {
                System.out.printf("%d: %s\n", i + 1, colleges.get(i).name);
            }
            System.out.print("\nEnter college number: ");
            int cmd = Helper.getIntInput() - 1;
            if (cmd >= 0 && cmd < size) {
                return colleges.get(cmd);
            }
            System.out.println("Enter valid number!\n");
        }
    }

    public static List<College> getColleges() {
        return colleges;
    }

    public static void addCollege(College college) {
        colleges.add(college);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addSemester(Semester semester) {
        semesters.add(0, semester);
    }

    public List<Lesson> getCurrentLessons() {
        if (semesters.size() > 0)
            return semesters.get(0).getLessons();
        return new ArrayList<>();
    }

    public int getCurrentSemesterID() {
        return currentSemesterID;
    }

    public void setCurrentSemesterID(int currentSemesterID) {
        this.currentSemesterID = currentSemesterID;
    }
}