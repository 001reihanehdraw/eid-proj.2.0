package src.menu;

import static src.Helper.getInput;
import static src.Helper.getIntInput;
import static src.Helper.getLongInput;

import java.util.ArrayList;
import java.util.List;

import src.Helper;
import src.models.College;
import src.models.Lesson;
import src.models.Professor;
import src.models.Student;

public class ProfessorMenu
{
    private static Professor professor;

    public static Professor mainMenu(Professor professor) {
        ProfessorMenu.professor = professor;
        System.out.println("\nWelcome " + professor.getName() + "!\n");
        while (true) {
            System.out.println("-------------- Professor Menu --------------");
            System.out.println();
            System.out.println("Courses:    For show courses");
            System.out.println("Edit Info:  To change user info");
            System.out.println("Logout:     To log out");
            System.out.println();

            System.out.print("Enter your command: ");
            String cmd = getInput();
            System.out.println();
            if (cmd.equals("Logout")) break;
            switch (cmd) {
                case "Courses":
                    showCourses();
                    break;
                case "Edit Info":
                    editInfo();
                    break;
            }
            System.out.println("\nPress enter to continue...");
            Helper.getInput();
        }
        return ProfessorMenu.professor;
    }

    private static void showCourses() {
        College college = professor.getCollege();
        List<Lesson> lessons = new ArrayList<>();
        List<Lesson> currentLessons = college.getCurrentLessons();
        if (currentLessons.size() > 0) {
            for (int i = 0; i < currentLessons.size(); i++) {
                Lesson l = currentLessons.get(i);
                if (l.getProfessor() == professor) {
                    lessons.add(l);
                }
            }
            while (true) {
                System.out.println("Choose a lesson:");
                for (int i = 0; i < lessons.size(); i++) {
                    Lesson l = lessons.get(i);
                    System.out.printf("%d: (%d) %s (%d)\n", i + 1, l.getLessonID(), l.getName(), l.getUnit());
                }
                System.out.print("\nType lesson number (Type 0 to go back): ");
                int intInput = Helper.getIntInput();

                if (intInput == 0) {
                    break;
                }
                intInput--;
                System.out.println();
                if (intInput < 0 || intInput >= lessons.size()) {
                    System.out.println("Invalid number!\n");
                    continue;
                }
                showStudents(lessons.get(intInput));
            }
        }
    }

    private static void showStudents(Lesson l) {
        List<Student> students = professor.getStudents(l);
        if (students.size() <= 0) {
            System.out.println("There's no students!");
            System.out.println("\nPress enter to continue...");
            Helper.getInput();
            return;
        }
        while (true) {
            System.out.println("Choose a student:");
            for (int i = 0; i < students.size(); i++) {
                Student s = students.get(i);
                System.out.printf("%d: (%d) %s\n", i + 1, s.getStudentID(), s.getName());
            }
            System.out.print("\nType student number (Type 0 to go back): ");
            int intInput = Helper.getIntInput();
            System.out.println();

            if (intInput == 0) {
                break;
            }
            intInput--;
            if (intInput < 0 || intInput >= students.size()) {
                System.out.println("Invalid number!\n");
                continue;
            }
            setGrade(students.get(intInput), l);
        }
    }

    private static void setGrade(Student student, Lesson l) {
        while (true) {
            System.out.printf("Enter %s's grade: ", student.getName());
            float grade = Helper.getFloatInput();
            if (grade >= 0 && grade <= 20) {
                student.addGrade(l, grade);
                System.out.println("Done!\n");
                break;
            } else {
                System.out.println("Invalid grade!\n");
            }
        }
    }

    private static void editInfo() {
        while (true) {
            System.out.println("Edit professor info:");
            int i = 0;
            System.out.printf("%d: (%s) %s\n", ++i, "Name", professor.getName());
            System.out.println();

            System.out.print("Type info number (type 0 to go back): ");
            int intInput = Helper.getIntInput();

            if (intInput == 0) {
                break;
            }
            System.out.println();
            if (intInput == 1) {
                System.out.print("Enter Your Name: ");
                professor.setName(getInput());
                System.out.println();
            }
        }
    }
}
